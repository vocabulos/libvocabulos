The Hunspell Format                                          {#hunspell-format}
===================

There is official documentation about the Hunspell file format available in
[PDF](https://downloads.sourceforge.net/project/hunspell/Hunspell/Documentation/hunspell4.pdf)
and [man](https://linux.die.net/man/4/hunspell) formats.

The official documentation is quite detailed, however it is not meant to serve
as reference for the implementation of software that generates Hunspell files
automatically, and it lacks some details that you should know if you plan to
contribute to this library or any other similar project.

The goal of this documentation page is to describe those additional details, as
an addendum to the official documentation linked above.

Escaping Slashes (/)
--------------------

Slashes that are part of a word, as opposed to the beginning of the list of
flags associated with that word, must be escaped as `\/`.

For example, ‘AC/DC’ would appear as `AC\/DC` in a dictionary file.

**Note:** This is actually explained in the official documentation, however
both the PDF and the man version fail to properly escape the `\` character, so
you have to [read the documentation
sources](https://github.com/hunspell/hunspell/blob/master/man/hunspell.5) to
find out.


Multi-Word Dictionary Entries
-----------------------------

Hunspell performs spellchecking in a word-by-word basis, it cannot check
multiple words in a row as a single entity.

For example, if you want your Hunspell files to support ‘ad hoc’, you must add
two entries to your dictionary file: `ad` and `hoc`. Of course, if you do that,
both ‘ad’ and ‘hoc’ will be considered valid words even when they do not appear
together.

To enforce such a restriction, consider using a grammar checker such as
[LanguageTool](https://www.languagetool.org/) instead of a spellchecker like
Hunspell.


Default Character Set
---------------------

If no character set is specified in the affix file, [ISO8859-1 is used](
https://github.com/hunspell/hunspell/blob/master/src/hunspell/csutil.hxx#L97).


Valid Extended ASCII Flag Characters
------------------------------------

When using extended ASCII characters as flags, all extended ASCII characters
may be used except for those in the following table:

| Hex | Escape Sequence | Name            |
| --: | --------------: | --------------- |
|  00 |             \\0 | Null            |
|  09 |             \\t | Horizontal Tab  |
|  0A |             \\n | Line Feed       |
|  0D |             \\r | Carriage Return |
|  20 |                 | Space           |
|  7F |                 | Delete          |

As long as the human readability of the resulting Hunspell files is not a
concern, all other 251 ASCII characters, including control characters, can be
used.


Maximum Numeric Flag
--------------------

The maximum number that you can use when using the numeric flag type is
**65509**, not 65000 as the documentation states.

The upper limit is [defined in csutil.hxx](
https://github.com/hunspell/hunspell/blob/master/src/hunspell/csutil.hxx#L124)
and [enforced in hashmgr.cxx](
https://github.com/hunspell/hunspell/blob/master/src/hunspell/hashmgr.cxx#L670).


Maximum Rules per Flag Type
---------------------------

| Type            | Code   | Max. Rules |
| --------------- | ------ | ---------: |
| ASCII           | (none) |        251 |
| Extended ASCII  | long   |     63,001 |
| Decimal Numbers | num    |     65,509 |
| UTF-8           | UTF-8  |  1,114,107 |
