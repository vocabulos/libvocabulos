// Copyright 2017 Adrián Chaves (Gallaecio) <adriyetichaves@gmail.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) version 3, or any
// later version accepted by the membership of KDE e.V. (or its
// successor approved by the membership of KDE e.V.), which shall
// act as a proxy defined in Section 6 of version 3 of the license.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library.  If not, see
// <http://www.gnu.org/licenses/>.

#include "vocabulos.h"

#include <set>

using namespace std;


// Source: http://stackoverflow.com/a/3418285
void replaceAll(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}

void vocabulos::wordsToHunspell(istream *words, ostream *aff, ostream *dic) {
    set<string> dicWords;
    {
        string word;
        while(getline(*words, word)) {
            replaceAll(word, "/", "\\/");
            dicWords.insert(word);
        }
    }
    int wordCount(dicWords.size());
    if (dicWords.size() == 0) {
        return;
    }
    *dic << wordCount << endl;
    bool firstLine(true);
    for(auto word : dicWords) {
        if (firstLine) {
            firstLine = false;
        } else {
            *dic << endl;
        }
        *dic << word;
    }
};
