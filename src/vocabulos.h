// Copyright 2017 Adrián Chaves (Gallaecio) <adriyetichaves@gmail.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) version 3, or any
// later version accepted by the membership of KDE e.V. (or its
// successor approved by the membership of KDE e.V.), which shall
// act as a proxy defined in Section 6 of version 3 of the license.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library.  If not, see
// <http://www.gnu.org/licenses/>.

#ifndef VOCABULOS_H
#define VOCABULOS_H

#include <iostream>

/**
 * Root namespace of libvocabulos
 */
namespace vocabulos {

/**
 * Reads a list of words (1 word per line) from the specified input stream and
 * fills the specified output streams with the contents of a set of Hunspell
 * files that support only the specified input words.
 */
void wordsToHunspell(std::istream *words, std::ostream *aff,
                     std::ostream *dic);

} // namespace vocabulos

#endif // VOCABULOS_H
