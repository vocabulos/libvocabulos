// Copyright 2017 Adrián Chaves (Gallaecio) <adriyetichaves@gmail.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) version 3, or any
// later version accepted by the membership of KDE e.V. (or its
// successor approved by the membership of KDE e.V.), which shall
// act as a proxy defined in Section 6 of version 3 of the license.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library.  If not, see
// <http://www.gnu.org/licenses/>.

#include "catch.hpp"

#include <vocabulos.h>

using namespace std;
using namespace vocabulos;

void test(const string &inputWords, const string &expectedAff,
          const string &expectedDic) {
    istringstream words(inputWords);
    ostringstream aff;
    ostringstream dic;
    wordsToHunspell(&words, &aff, &dic);
    REQUIRE(expectedAff == aff.str());
    REQUIRE(expectedDic == dic.str());
}

TEST_CASE("0 words") {
    test("", "", "");
}

TEST_CASE("1 word") {
    test("a", "", "1\na");
}

TEST_CASE("2 words") {
    test("a\nb", "", "2\na\nb");
}

TEST_CASE("Words get sorted on the .dic file") {
    test("b\na", "", "2\na\nb");
}

TEST_CASE("Duplicate words are removed") {
    test("a\na\na", "", "1\na");
}

TEST_CASE("Slashes are escaped") {
    test("/", "", "1\n\\/");
}
