`libvocabulos` is a C++ library that can generate the smallest set of Hunspell
files out of a given list of words.

This library may be used under the terms of the **LGPL** version 2.1, or
version 3 or later versions approved by the membership of
[KDE e.V.](https://ev.kde.org/).


Dependencies
============

We use [CMake](https://cmake.org/) as our build system, so it is required at
build time.


Building and Installing the Library
===================================

To build the library:

1.  Prepare a build folder with CMake:

        mkdir build
        cd build
        cmake ..

2.  Build the project:

        make

3.  Install the project:

        sudo make install


Building the Documentation
==========================

To build the documentation:

1.  Prepare a build folder with CMake:

        mkdir build
        cd build
        cmake ..

2.  Build the `doc` target:

        make doc

You can open `doc/index.html` in a web browser to read the documentation.

If you cannot build the documentation, see `src/vocabulos.h` for documentation
in the form of source comments.


API Reference
=============

See the [vocabulos namespace documentation](@ref vocabulos).

**Note:** The link above only works on the built documentation, see
**Building the Documentation**.


Contributing
============

To contribute code to the project, you must:

-   Agree to the terms of the license.

-   Ensure that your changes do not break existing code. To check, run
    `autotests/autotests` on your build folder.

We also appreciate it if you:

-   Update the automated tests to provide coverage for your changes.

    Run the following in your build folder to get a code coverage report:

        cmake -DCMAKE_BUILD_TYPE=Debug ..
        make coverage

    You can open `coverage/index.html` in a web browser to read the code
    coverage report.

-   Update the documentation affected by your code changes.

    Run the following in your build folder to get a documentation coverage
    report:

        make doc
        python3 -m venv venv
        . venv/bin/activate
        pip install coverxygen
        python -m coverxygen --xml-dir doc/coverage/ --output doc-coverage.info --src-dir ..
        genhtml --no-function-coverage --no-branch-coverage doc-coverage.info -o doc/coverage

    You can open `doc-coverage/index.html` in a web browser to read the
    documentation coverage report.


See Also
========

-   [The Hunspell Format](@ref hunspell-format)
